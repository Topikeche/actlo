<?php if ( ! defined('BASEPATH')) exit('No direct script acces allowed'); 
/**
* Login Controller
* Create by Rochmad Handoko Aji
* Seintica Production
* ajiilkom@ymail.com
*/
class Admin extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form','html','text'));
		$this->load->library('session');
		$this->load->database();
	}

	function index(){
		$data['title'] = "Administrator";
		$this->load->view('login', $data, FALSE);
	}

	function proses(){
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$periksa = $this->db->from('admin')->where('username', $username)->where('password', $password)->get();

		if ($periksa->num_rows() == 1) {
			$id = $periksa->result_array();
			$this->session->set_userdata('login', TRUE);
			$this->session->set_userdata('id', $id[0]['id']);
			redirect('admin/dashboard');
		}else{
			echo("Username atau password salah");
			redirect('admin');
		}
	}

	function dashboard(){
		if ($this->session->userdata('login') == TRUE) {
			$data['title'] = "Selamat datang Admin !";
			$data['nav'] = $this->load->view('nav', NULL, TRUE);
			$data1['daftar'] = $this->db->select('*')->from('post')->get()->result();
			$data['content'] = $this->load->view('daftar_post', $data1, TRUE);
			$this->load->view('dashboard', $data, FALSE);
		}else{
			redirect('admin');
		}
	}
	function posting(){
		if ($this->session->userdata('login') == TRUE) {
			$data['title'] = "Posting !";
			$data['nav'] = $this->load->view('nav', NULL, TRUE);
			$data['content'] = $this->load->view('posting', NULL, TRUE);
			$this->load->view('dashboard', $data, FALSE);
		}else{
			redirect('admin');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('admin');
	}

	function post(){
		if ($this->session->userdata('login') == TRUE) {
			$judul = $this->input->post('judul');
			$isi = $this->input->post('isi');
			$category = $this->input->post('category');

			$this->db->insert('post', array('judul' =>$judul, 'isi'=>$isi, 'category'=>$category));
			redirect('admin/dashboard');
		}
	}

	function hapus($id){
		if ($this->session->userdata('login') == TRUE) {
			$this->db->where('id',$id)->delete('post');

			redirect('admin/dashboard');
		}else{
			redirect('admin');
		}
	}

	function ubah($id){
		if ($this->session->userdata('login') == TRUE) {
			$data['title'] = "Edit Posting !";
			$data['ubah'] = $this->db->select('*')->from('post')->where('id', $id)->get()->result();
			$data['nav'] = $this->load->view('nav', NULL, TRUE);
			$data['content'] = $this->load->view('ubah_view',$data, TRUE);
			$this->load->view('dashboard', $data, FALSE);	
		}else{
			redirect('admin');
		}
	}

	function proses_ubah(){
		if ($this->session->userdata('login') == TRUE) {
			$id = $this->input->post('id');
			$judul = $this->input->post('judul');
			$isi = $this->input->post('isi');
			$category = $this->input->post('category');
			$this->db->where('id', $id)->update('post', array('judul'=>$judul, 'isi'=>$isi, 'category'=>$category));
			redirect('admin/dashboard');
		}
	}
}
?>