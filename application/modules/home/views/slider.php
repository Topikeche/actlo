<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php 
      $jumlah_slide = $this->db->from('galery')->count_all_results();
      
      for ($j=0; $j < $jumlah_slide; $j++) { 
        
      

      if ($j == 0) {
        echo"
          <li data-target='#carousel-example-generic' data-slide-to='$j' class='active'></li>    
        ";
      }else{
        echo"
          <li data-target='#carousel-example-generic' data-slide-to='$j'></li>
        ";
      }
    }
    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    
    <?php 
      $i = 0;
      foreach ($gambar_slider as $gambar) :
        if ($i == 0) {
          echo"
        <div class='item active'>
              <img src='img/slider/{$gambar->image_name}' alt=''>
            </div>
      "; 
        }else{
          echo"
        <div class='item'>
              <img src='img/slider/{$gambar->image_name}' alt=''>
            </div>
      ";
        }

      $i++;
      endforeach;
    ?>
  </div>
  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>